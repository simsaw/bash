#!/bin/sh

INSTALL_DIR=/opt/local/etc/nginx
NAME=nginx-1.4.4
TAR_FILE_NAME=$NAME.tar.gz

CONF_DIR=/root/nginx-conf

echo "==="
echo "Install $NAME"
echo "==="

cd ~/

echo "*** download $NAME source"
wget http://nginx.org/download/$TAR_FILE_NAME

echo "** extracting $NAME source ==="
tar xvzpf $TAR_FILE_NAME

cd ~/$NAME

echo "*** configure $NAME"

CC="gcc" ./configure \
--user=www \
--group=www \
--with-ld-opt='-L/opt/local/lib -Wl,-R/opt/local/lib' \
--prefix=/opt/local \
--sbin-path=/opt/local/sbin \
--conf-path=/opt/local/etc/nginx/nginx.conf \
--pid-path=/var/db/nginx/nginx.pid \
--lock-path=/var/db/nginx/nginx.lock \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--http-client-body-temp-path=/var/db/nginx/client_body_temp \
--http-proxy-temp-path=/var/db/nginx/proxy_temp \
--http-fastcgi-temp-path=/var/db/nginx/fstcgi_temp \
--with-http_realip_module \
--with-ipv6 \
--with-http_ssl_module \
--with-http_stub_status_module \
--with-http_gzip_static_module

echo "*** make $NAME"
make
echo "*** make install $NAME"
make install

cd ~/
. ~/.bash_profile

echo "*** creating directories {sites-enabled, certs, log/nginx, spool/nginx} ==="
mkdir -p $INSTALL_DIR/sites-enabled
mkdir /var/log/nginx
mkdir /var/spool/nginx

echo "*** rm $NAME source code dir"
rm $INSTALL_DIR/nginx.conf

echo "=== importing and start service nginx ==="
svccfg -v import $CONF_DIR/smf.xml
svcadm enable nginx

echo "=== remove unwanted stuff ==="
rm -rf ~/$NAME
rm -rf ~/$TAR_FILE_NAME


echo "=== DONE ==="
