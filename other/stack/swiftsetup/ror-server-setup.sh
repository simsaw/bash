# passanger package setup
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 561F9B9CAC40B2F7

sudo touch  /etc/apt/sources.list.d/passenger.list
	# Ubuntu 14.04
	deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main

	# Ubuntu 12.04
	# deb https://oss-binaries.phusionpassenger.com/apt/passenger precise main

sudo chown root: /etc/apt/sources.list.d/passenger.list
sudo chmod 600 /etc/apt/sources.list.d/passenger.list
sudo apt-get update

# install nginx and passanger
sudo apt-get install nginx nginx-extras passenger


# create deploy user

# install JS runtime env nodejs
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs

### install ruby  ###
# install required libs
sudo apt-get install git-core /
curl /
zlib1g-dev /
build-essential /
libssl-dev /
libreadline-dev /
libyaml-dev /
libsqlite3-dev /
sqlite3 /
libxml2-dev /
libxslt1-dev /
libcurl4-openssl-dev /
python-software-properties

# using rbenv
#git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
#echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
#echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
#. ~/.bashrc
#
#rbenv install 2.1.2
#rbenv global 2.1.2
#ruby -v

sudo gem install bundler
sudo apt-get install libmysqlclient-dev