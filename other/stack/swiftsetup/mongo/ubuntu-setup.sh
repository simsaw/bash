#!/bin/sh

# ref http://docs.mongodb.org/ecosystem/platforms/amazon-ec2/
# create a instance with xvdf: 200gb/1000io xvdg: 25gb/250io xvdh: 10gb/10io
# ssh to server and run following cmd

sudo apt-get update
sudo apt-get -y upgrade

sudo apt-get -y install git build-essential

# install mongodb
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
touch mongo.list
echo "deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen" >> mongo.list
sudo mv mongo.list /etc/apt/sources.list.d/10gen.list
sudo apt-get update
sudo apt-get install mongodb-10gen

# enable to start on reboot
sudo update-rc.d mongodb defaults

# create dir
sudo mkdir /data /log /journal
sudo mkfs.ext4 /dev/xvdf
sudo mkfs.ext4 /dev/xvdg
sudo mkfs.ext4 /dev/xvdh

echo '/dev/xvdf /data ext4 defaults,auto,noatime,noexec 0 0
/dev/xvdg /journal ext4 defaults,auto,noatime,noexec 0 0
/dev/xvdh /log ext4 defaults,auto,noatime,noexec 0 0' | sudo tee -a /etc/fstab

sudo mount /data
sudo mount /journal
sudo mount /log

sudo chown -R mongodb:mongodb /data /journal /log

sudo ln -s /journal /data/journal

sudo blockdev --setra 32 /dev/xvdf

echo 'ACTION=="add", KERNEL=="xvdf", ATTR{bdi/read_ahead_kb}="16"' | sudo tee -a /etc/udev/rules.d/85-ebs.rules

#  custom editing ==>

echo "dbpath = /data" > ./mongodb.conf
echo "logpath = /log/mongod.log" >> ./mongodb.conf
echo "logappend=true" >> ./mongodb.conf
echo "journal = true" >> ./mongodb.conf
echo "nohttpinterface = true" >> ./mongodb.conf
echo "replSet = simsaw" >> ./mongodb.conf
echo "port = 27017" >> ./mongodb.conf
echo "directoryperdb = true" >> ./mongodb.conf
sudo cp /etc/mongodb.conf /etc/mongodb.conf.orig
sudo mv ./mongodb.conf /etc/mongodb.conf


echo "* soft nofile 64000" > ./limits.conf
echo "* hard nofile 64000" >> ./limits.conf
echo "* soft nproc 32000" >> ./limits.conf
echo "* hard nproc 32000" >> ./limits.conf
sudo cp /etc/security/limits.conf /etc/security/limits.conf.orig
sudo mv ./limits.conf /etc/security/limits.conf

echo "* soft nproc 32000" > ./90-nproc.conf
echo "* hard nproc 32000" >> ./90-nproc.conf
sudo cp /etc/security/limits.d/90-nproc.conf /etc/security/limits.d/90-nproc.conf.orig
sudo mv ./90-nproc.conf /etc/security/limits.d/90-nproc.conf

sudo service mongodb restart