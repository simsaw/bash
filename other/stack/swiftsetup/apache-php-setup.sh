#!/bin/bash

sudo apt-get -y update
sudo apt-get -y upgrade

sudo apt-get -y install apache2
sudo apt-get -y install php5 libapache2-mod-php5 php5-mcrypt php5-mysql libapache2-mod-auth-mysql 
sudo apt-get -y install curl libcurl3 libcurl3-dev php5-curl
sudo a2enmod rewrite

sudo service apache2 restart
