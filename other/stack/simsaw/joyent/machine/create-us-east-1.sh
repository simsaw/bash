#!/bin/sh

NAME=$1
IMAGE=$2

SCRIPT_PATH=${BASH_SOURCE[0]}
BASE_DIR=$(dirname $SCRIPT_PATH)

sh $BASE_DIR/create.sh https://us-east-1.api.joyentcloud.com $NAME  $IMAGE