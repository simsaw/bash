#!/bin/sh

DC=$1
NAME=$2

SCRIPT_PATH=${BASH_SOURCE[0]}
BASE_DIR=$(dirname $SCRIPT_PATH)

#6a6c0056-38b9-4982-90ad-682d63970b4c  Micro 0.6 GB RAM 0.15 vCPU and bursting 20 GB Disk
#06a0251c-038f-4eda-8af2-653c46b3aee8  Micro 0.5 GB RAM 0.125 vCPU and bursting 16 GB Disk
#d71da280-92da-489f-9c4c-c91891fa202f  Micro 0.25 GB RAM 0.125 vCPU and bursting 16 GB Disk

PACKAGE=${3:- 6a6c0056-38b9-4982-90ad-682d63970b4c} # Default size

IMAGE=${4:- 0084dad6-05c1-11e3-9476-8f8320925eea} # set default image base64

echo "*** Create machine"
echo "$NAME, data-center: $DC, image: $IMAGE"
echo "***"

sdc-createmachine -n $NAME -e $IMAGE --url $DC --package $PACKAGE \
--tag stack="simsaw" -s $BASE_DIR/create.sh
