#!/bin/sh
set -e

NAME=monit-5.6
TAR_FILE=monit-5.6.tar.gz

cd ~/

echo "*** Download source"
wget https://mmonit.com/monit/dist/$TAR_FILE
tar xvf $TAR_FILE

echo "*** Install Monit"
cd ~/$NAME
./configure --with-ssl-incl-dir
make
make install

#cp monitrc /etc/monitc

echo "*** Remove source files"
cd ~/
rm -rf ~/$NAME