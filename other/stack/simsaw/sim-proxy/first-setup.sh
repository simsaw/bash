#!/bin/sh
cd ~/

pkgin update
pkgin upgrade
pkgin -y in py27-expat-2.7.5

wget http://kaz.dl.sourceforge.net/project/s3tools/s3cmd/1.5.0-beta1/s3cmd-1.5.0-beta1.tar.gz
tar xvf s3cmd-1.5.0-beta1.tar.gz
rm xvf s3cmd-1.5.0-beta1.tar.gz
cd ~/s3cmd-1.5.0-beta1
python setup.py install
cd ~/


s3cmd --configure
#chmod 644 .s3cfg

s3cmd sync -r -P \
--access_key=AKIAILHUNAOI2AD6G56Q \
--secret_key=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 \
s3://ops.client.simsaw.com/simsaw/sim-proxy/ /root

mkdir -p /root/www
groupadd www
useradd -g www www

echo "*** set ip filter stuff"
PUBLIC_IP=$(ifconfig | grep broadcast |  awk '{split($0,a); print a[2]}' | xargs | awk '{split($0,a); print a[2]}')
PRIVATE_IP=$(ifconfig | grep 10.112.8 | awk '{split($0,a); print a[2]}' | xargs)

svccfg -s network/ipfilter:default setprop firewall_config_default/policy = astring: custom
svccfg -s network/ipfilter:default setprop firewall_config_default/custom_policy_file = astring: "/etc/ipf/ipf.conf"

echo "# Public IP" > "/etc/ipf/ipf.conf"
echo "block in from any to $PUBLIC_IP" >> "/etc/ipf/ipf.conf"
echo "pass in quick from any to $PUBLIC_IP port=22" >> "/etc/ipf/ipf.conf"
echo "pass in quick proto tcp from any to $PUBLIC_IP port=443" >> "/etc/ipf/ipf.conf"
echo "pass in quick proto tcp from any to $PUBLIC_IP port=80" >> "/etc/ipf/ipf.conf"

echo "# Private IP" >> "/etc/ipf/ipf.conf"
echo "block in from any to $PRIVATE_IP" >> "/etc/ipf/ipf.conf"
echo "pass out from $PRIVATE_IP to any keep state" >> "/etc/ipf/ipf.conf"
echo "pass in quick from 10.112.8.0/25 to $PRIVATE_IP" >> "/etc/ipf/ipf.conf"


echo "*** enable ipfilter service"
svcadm refresh ipfilter
svcadm enable ipfilter