upstream simsaw_baas {
    server 10.112.8.14:3301 weight=10;
    server 10.112.8.12:3301 weight=10;
}
server {
        listen 80;
        listen 443 ssl;
        ssl_certificate         /root/nginx-conf/certs/simsaw.com.bundle.crt;
        ssl_certificate_key     /root/nginx-conf/certs/simsaw.key;
        ssl_prefer_server_ciphers   on;


        server_name baas.simsaw.com;

        if ( $scheme = "http" ) {
            rewrite  ^/(.*)$  https://baas.simsaw.com/$1 permanent;
        }

        location / {
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header   X-Forwarded-Proto $scheme;
          proxy_set_header Host $host;
          proxy_set_header   Connection "";
          proxy_http_version 1.1;
          proxy_set_header X-NginX-Proxy true;
          proxy_redirect off;
          keepalive_timeout 30;
          proxy_pass http://simsaw_baas;
        }
    }