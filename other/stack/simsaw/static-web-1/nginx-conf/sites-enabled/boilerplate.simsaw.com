server {
    listen 80;

    server_name boilerplate.simsaw.com;

    root /root/www/boilerplate.simsaw.com;
    index  index.html;

    access_log /var/log/nginx/boilerplate-simsaw.log main;

    if ($args ~ "_escaped_fragment_=/?(.+)") {
        rewrite ^ /seo/boilerplate.simsaw.com;
    }

    location /seo {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_set_header Connection "";
        proxy_http_version 1.1;
        proxy_set_header X-NginX-Proxy true;
        proxy_redirect off;
        keepalive_timeout 30;
        proxy_pass http://snapshot_stream;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
        expires 30d;
        add_header Pragma public;
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";
    }
}