server {
    listen 80;

    server_name dogsenseunleashed.com www.dogsenseunleashed.com;

    root /root/www/dogsenseunleashed.com;
    index  index.html;

    access_log /var/log/nginx/dogsenseunleashed.log main;

    if ($host = 'dogsenseunleashed.com' ) {
        rewrite  ^/(.*)$  http://www.dogsenseunleashed.com/$1 permanent;
    }

    if ($args ~ "_escaped_fragment_=/?(.+)") {
        rewrite ^ /seo/dogsenseunleashed.com;
    }

    location /seo {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_set_header Connection "";
        proxy_http_version 1.1;
        proxy_set_header X-NginX-Proxy true;
        proxy_redirect off;
        keepalive_timeout 30;
        proxy_pass http://snapshot_stream;
    }


    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
        expires 30d;
        add_header Pragma public;
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";
    }
}