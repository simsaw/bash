#!/bin/sh

conf_dir=/root/nginx-conf
install_path=/opt/local/etc/nginx
source_dir=nginx-1.5.9
tar_file_name=$source_dir.tar.gz

echo "=== cd to home dir ==="
cd ~/

echo "=== update package ==="
pkgin update

echo "=== install build-essential ==="
pkgin -y install build-essential

echo "=== Download nginx source ==="
wget http://nginx.org/download/$tar_file_name

echo "=== extracting nginx source ==="
tar xvzpf $tar_file_name

echo "=== cd to nginx source dir ==="
cd ~/$source_dir

echo "=== configure nginx ==="
CC="gcc" ./configure \
--user=www \
--group=www \
--with-ld-opt='-L/opt/local/lib -Wl,-R/opt/local/lib' \
--prefix=/opt/local \
--sbin-path=/opt/local/sbin \
--conf-path=/opt/local/etc/nginx/nginx.conf \
--pid-path=/var/db/nginx/nginx.pid \
--lock-path=/var/db/nginx/nginx.lock \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--http-client-body-temp-path=/var/db/nginx/client_body_temp \
--http-proxy-temp-path=/var/db/nginx/proxy_temp \
--http-fastcgi-temp-path=/var/db/nginx/fstcgi_temp \
--with-http_realip_module \
--with-ipv6 \
--with-http_ssl_module \
--with-http_stub_status_module \
--with-http_gzip_static_module

#--with-http_dav_module \
#--with-mail_ssl_module \
#--add-module=/root/echo-nginx-module

echo "=== make nginx ==="
make
echo "=== make install nginx  ==="
make install

echo "=== cd to home dir ==="
cd ~/
. ~/.bash_profile

echo "=== creating directories {sites-enabled, certs, log/nginx, spool/nginx} ==="
mkdir -p $install_path/sites-enabled
mkdir /var/log/nginx
mkdir /var/spool/nginx

rm $install_path/nginx.conf
ln -s $conf_dir/nginx.conf $install_path/nginx.conf

echo "=== symlink sites-enabled ==="
for i in $conf_dir/sites-enabled/*
do
    ln -s $i $install_path/sites-enabled/$(basename $i)
done

echo "=== importing and start service nginx ==="
svccfg -v import $conf_dir/smf.xml
svcadm enable nginx

echo "=== remove unwanted stuff ==="
rm -rf ~/$source_dir
rm -rf ~/$tar_file_name


echo "=== DONE ==="