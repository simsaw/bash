#!/bin/sh


svccfg -s network/ipfilter:default setprop firewall_config_default/policy = astring: custom

svccfg -s network/ipfilter:default setprop firewall_config_default/custom_policy_file = astring: "/etc/ipf/ipf.conf"

svcadm refresh ipfilter

svcadm enable ipfilter
