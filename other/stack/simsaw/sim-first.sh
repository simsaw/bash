#!/bin/sh

echo "*** install required packages"
pkgin -y update
pkgin -y upgrade
pkgin -y install build-essential git


echo "*** set ip filter stuff"
PUBLIC_IP=$(ifconfig | grep broadcast |  awk '{split($0,a); print a[2]}' | xargs | awk '{split($0,a); print a[2]}')
PRIVATE_IP=$(ifconfig | grep 10.112.8 | awk '{split($0,a); print a[2]}' | xargs)

svccfg -s network/ipfilter:default setprop firewall_config_default/policy = astring: custom
svccfg -s network/ipfilter:default setprop firewall_config_default/custom_policy_file = astring: "/etc/ipf/ipf.conf"

echo "# Public IP" > "/etc/ipf/ipf.conf"
echo "block in from any to $PUBLIC_IP" >> "/etc/ipf/ipf.conf"
echo "pass in quick from any to $PUBLIC_IP port=22" >> "/etc/ipf/ipf.conf"
echo "pass out from $PUBLIC_IP to any keep state" >> "/etc/ipf/ipf.conf"

echo "# Private IP" >> "/etc/ipf/ipf.conf"
echo "block in from any to $PRIVATE_IP" >> "/etc/ipf/ipf.conf"
echo "pass out from $PRIVATE_IP to any keep state" >> "/etc/ipf/ipf.conf"
echo "pass in quick from 10.112.8.0/25 to $PRIVATE_IP" >> "/etc/ipf/ipf.conf"

#echo "pass in quick proto tcp from any to $PUBLIC_IP port=443" >> "/etc/ipf/ipf.conf"
#echo "pass in quick proto tcp from any to $PUBLIC_IP port=80" >> "/etc/ipf/ipf.conf"

#echo "pass in quick from server-mongo-p1.simsaw.com to $PUBLIC_IP port=27017" >> "/etc/ipf/ipf.conf"
#echo "pass in quick from server-mongo-s1.simsaw.com to $PUBLIC_IP port=27017" >> "/etc/ipf/ipf.conf"
#echo "pass in quick from server-mongo-s2.simsaw.com to $PUBLIC_IP port=27017" >> "/etc/ipf/ipf.conf"

echo "*** enable ipfilter service"
svcadm enable ipfilter
#svcadm refresh ipfilter


# new relic server monitoring
pkgin -y install nrsysmond
vim /opt/local/etc/nrsysmond.cfg #659979931e6cb109d98db415c8af8f2e505675b0
svcadm enable pkgsrc/nrsysmond

