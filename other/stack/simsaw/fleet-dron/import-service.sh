#!/bin/sh

cp /root/fleet-dron/fleet-dron.xml /var/svc/manifest/application/fleet-dron.xml
svccfg -v import /var/svc/manifest/application/fleet-dron.xml
chmod 444 /var/svc/manifest/application/fleet-dron.xml
chown root:sys /var/svc/manifest/application/fleet-dron.xml