#!/bin/sh

conf_dir=$1
install_path=/opt/nginx/conf
source_dir=nginx-1.4.4
tar_file_name=$source_dir.tar.gz

echo "===\n cd to home dir \n==="
cd ~/

echo "===\n update package \n==="
pkgin update

echo "===\n install build-essential \n==="
pkgin -y install build-essential

echo "===\n Download nginx source \n==="
wget http://nginx.org/download/$tar_file_name

echo "===\n extracting nginx source \n==="
tar xvzpf $tar_file_name

echo "===\n cd to nginx source dir \n==="
cd ~/$source_dir

echo "===\n configure nginx \n==="
CC="gcc" ./configure \
--prefix=/opt/nginx \
--with-cpu-opt="amd64" \
--with-http_ssl_module \
--with-http_gzip_static_module

echo "===\n make nginx \n==="
make
echo "===\n make install nginx  \n==="
make install

echo "===\n cd to home dir \n==="
cd ~/

echo "===\n creating directories {sites-enabled, certs, log/nginx, spool/nginx} \n==="
mkdir -p $install_path/sites-enabled
mkdir -p $install_path/certs
mkdir /var/log/nginx
mkdir /var/spool/nginx

echo "===\n copying files {nginx.conf, smf.xml}  \n==="
cp $conf_dir/nginx.conf $install_path/
cp $conf_dir/smf.xml /var/svc/manifest/network/nginx.xml

echo "===\n symlink sites-enabled \n==="
for i in $conf_dir/sites-enabled/*
do
    ln -s $i $install_path/sites-enabled/$(basename $i)
done

echo "===\n symlink certs \n==="
for i in $conf_dir/certs/*
do
    ln -s $i $install_path/certs/$(basename $i)
done

echo "===\n creating service nginx \n==="
svccfg -v import /var/svc/manifest/network/nginx.xml
chmod 444 /var/svc/manifest/network/nginx.xml
chown root:sys /var/svc/manifest/network/nginx.xml
echo "===\n starting nginx service \n==="
svcadm enable nginx

echo "===\n remove unwanted stuff \n==="
rm -rf ~/$source_dir
rm -rf ~/$tar_file_name

echo "===\n DONE \n==="