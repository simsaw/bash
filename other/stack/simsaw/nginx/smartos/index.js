var childProcess = require('child_process');

exports.install = function () {
    'use strict';

    var ls = childProcess.execFile(
        __dirname + '/sh/install-custom.sh',
        [__dirname + '/conf']);

    ls.stdout.on('data', function (data) {
        console.log(data);
    });

    ls.stderr.on('data', function (data) {
        console.error(data);
    });

    ls.on('close', function (code) {
        console.log('install nginx process exited with code ' + code);
    });
};