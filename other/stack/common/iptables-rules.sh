# install a service to persist rules
# ref https://www.digitalocean.com/community/articles/how-to-set-up-a-firewall-using-ip-tables-on-ubuntu-12-04
# list iptable rules: iptables -L
apt-get install iptables-persistent
service iptables-persistent start

# flush existing rules
iptables -F

iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport ssh -j ACCEPT
iptables -A INPUT -s 115.112.139.206 -p tcp --dport 4730 -j ACCEPT
iptables -A INPUT -s 10.112.8.0/25 -p all -j ACCEPT
iptables -A INPUT -j DROP

service iptables-persistent save