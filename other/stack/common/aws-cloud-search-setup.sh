#!/bin/sh

DIR=~/aws-cloud-search-tools

cd ~/

echo "install java on machine"
pkgin -y in sun-jre6-6.0.26

echo "*** Downloading source"
wget https://s3.amazonaws.com/amazon-cloudsearch-data/cloud-search-tools-1.0.2.3-2013.08.02.tar.gz $DIR.tar

echo "*** Extracting source code"
tar xvf $DIR.tar
rm $DIR.tar

echo 'export CS_HOME="$HOME/aws-cloud-search-tools"' >> ~/.bash_profile
echo 'export PATH="$PATH:$CS_HOME/bin"' >> ~/.bash_profile

# for mac
#echo 'export JAVA_HOME=$(/usr/libexec/java_home)' >> ~/.bash_profile

echo 'export JAVA_HOME=$(/opt/local/java/sun6/bin)' >> ~/.bash_profile


. ~/.bash_profile

