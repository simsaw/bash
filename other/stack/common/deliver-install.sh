#!/bin/sh

#ref: https://github.com/gerhard/deliver
echo "=== Doing git cline -> Deliver code"
git clone git://github.com/gerhard/deliver.git ~/.deliver
echo "=== Setting path"
echo 'export PATH="$HOME/.deliver/bin:$PATH"' >> ~/.bash_profile
. ~/.bash_profile
