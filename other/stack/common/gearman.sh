#!/bin/sh
DIR=~/gearmand

cd ~/
pkgin -y in build-essential boost gperf libevent ossp-uuid

wget --no-check-certificate https://launchpad.net/gearmand/1.2/1.1.11/+download/gearmand-1.1.11.tar.gz $DIR.tar.gz
tar xvf $DIR.tar.gz

cd $DIR
./configure
make
make install



