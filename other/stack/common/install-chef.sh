#!/bin/sh

# setup
gem install knife-solo --no-ri --no-rdoc
gem install chef --no-ri --no-rdoc

knife configure -r . --defaults


#kitchen
knife kitchen simsaw

#a coookbook
knife cookbook create nginx -o cookbooks

#prepaire a node
knife prepare user@0.0.0.0

knife bootstrap 72.2.115.99 -d smartos-gz-fat -r smartos -N smartos20