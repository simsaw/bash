sudo apt-get update
sudo apt-get -y upgrade

sudo apt-get -y install \
    python-software-properties \
    software-properties-common \
    git-core \
    build-essential \
    imagemagick \
    openssl \
    xorg \
    libssl-dev \
    libxrender-dev

sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs

wget http://jaist.dl.sourceforge.net/project/wkhtmltopdf/0.12.0/wkhtmltox-linux-amd64_0.12.0-03c001d.tar.xz
tar -xvf <file name>
sudo mv wkhtmltopdf/bin/wkhtmltopdf /usr/bin/
sudo chown root:root /usr/bin/wkhtmltopdf
sudo chmod +x /usr/bin/wkhtmltopdf
