echo "*** apt-get update and upgrade***"
sudo apt-get update
sudo apt-get -y upgrade

echo "*** install require lib ***"
sudo apt-get -y install \
    python-software-properties \
    software-properties-common \
    build-essential \
    git-core


echo "*** install nginx ***"
sudo add-apt-repository ppa:nginx/stable
sudo apt-get -y update
sudo apt-get -y install nginx

mkdir -p nginx-sites-enabled
sudo rm -rf /etc/nginx/sites-enabled
sudo ln -s /home/ubuntu/nginx-sites-enabled /etc/nginx/sites-enabled


echo "*** install node.js ***"
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs