server {
    listen 80;

    server_name simsaw.com www.simsaw.com;
    access_log /var/log/nginx/simsaw.log;

    root /home/ubuntu/www/simsaw.com/public;
    index  index.html;

    if ($host = 'simsaw.com' ) {
        rewrite  ^/(.*)$  http://www.simsaw.com/$1 permanent;
    }

    if ($args ~ "_escaped_fragment_=/?(.+)") {
        rewrite ^ /seo/simsaw.com;
    }

    location /seo {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_set_header Connection "";
        proxy_http_version 1.1;
        proxy_set_header X-NginX-Proxy true;
        proxy_redirect off;
        keepalive_timeout 30;
        proxy_pass http://snapshot_stream;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
        expires 30d;
        add_header Pragma public;
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";
    }
}