#!/usr/bin/env bash

# set -e -x
echo 'export LC_ALL="en_US.UTF-8"' >> ~/.bashrc

echo "=="
echo "==> Basic Setup =="
echo "=="
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install git-core build-essential openssl mysql-client libmysqlclient-dev libsqlite3-dev sqlite3 \
	zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev \
	libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev

echo "==> RVM and Ruby setup"
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm

echo "==> Install ruby 1.8.7-p370"
rvm install 1.8.7-p370
rvm use 1.8.7-p370
gem install bundler --no-ri --no-rdoc

echo "==> install nGinx"
sudo apt-get -y install nginx

# ref: https://www.phusionpassenger.com/documentation/Users%20guide%20Standalone.html#install_on_debian_ubuntu
echo "==> install passenger"
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 561F9B9CAC40B2F7
sudo apt-get install apt-transport-https ca-certificates
echo "deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main" > passenger.list
sudo mv passenger.list /etc/apt/sources.list.d/
sudo chown root: /etc/apt/sources.list.d/passenger.list
sudo chmod 600 /etc/apt/sources.list.d/passenger.list
sudo apt-get update
sudo apt-get -y install nginx-extras passenger
# Edit /etc/nginx/nginx.conf and uncomment passenger_root and passenger_ruby.
#   passenger_ruby /home/ubuntu/.rvm/gems/ruby-1.8.7-p370/wrappers/ruby;
sudo service nginx restart

echo "==>"
echo "==> Code Deploy"
echo "==>"

echo "==> Install ruby-2.2.2"
rvm install ruby-2.2.2
rvm use ruby-2.2.2
# install pip
sudo apt-get -y install  python-pip
echo "==> install aws cli"
sudo pip install awscli
echo "==> Intsalling AWS code deploy-agent, service: codedeploy-agent"
aws s3 cp s3://aws-codedeploy-us-east-1/latest/install ./install-aws-codedeploy-agent --region us-east-1
chmod +x ./install-aws-codedeploy-agent
sudo ./install-aws-codedeploy-agent auto
rm install-aws-codedeploy-agent