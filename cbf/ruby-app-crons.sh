#!/usr/bin/env bash

echo 'export LC_ALL="en_US.UTF-8"' >> ~/.bashrc

echo "=="
echo "==> Basic Setup =="
echo "=="
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install git-core build-essential openssl mysql-client libmysqlclient-dev libsqlite3-dev sqlite3 \
	zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev \
	libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev

echo "==> RVM and Ruby setup"
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm

echo "==> Install ruby 1.8.7-p370"
rvm install 1.8.7-p370
rvm use 1.8.7-p370
gem install bundler --no-ri --no-rdoc

echo "==>"
echo "==> Code Deploy"
echo "==>"

echo "==> Install ruby-2.2.2"
rvm install ruby-2.2.2
rvm use ruby-2.2.2
# install pip
sudo apt-get -y install  python-pip
echo "==> install aws cli"
sudo pip install awscli
echo "==> Intsalling AWS code deploy-agent, service: codedeploy-agent"
aws s3 cp s3://aws-codedeploy-us-east-1/latest/install ./install-aws-codedeploy-agent --region us-east-1
chmod +x ./install-aws-codedeploy-agent
sudo ./install-aws-codedeploy-agent auto
rm install-aws-codedeploy-agent

echo "==> Install postfix"
sudo apt-get -y install postfix

echo "root: 'ankit@simsaw.com'" >> /etc/aliases
echo "ubuntu: 'ankit@simsaw.com'" >> /etc/aliases
