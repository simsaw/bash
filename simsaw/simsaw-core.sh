#!/bin/sh

export LC_ALL="en_US.UTF-8"

echo "==> yum update"
yum update -y

echo "==> Intsalling required packages"
yum install -y git-core gcc gcc-c++ make openssl-devel
yum install -y ImageMagick

echo "==> installint wkhtmltopdf"
yum install -y xorg-x11-fonts-75dpi
rpm -ivh http://liquidtelecom.dl.sourceforge.net/project/wkhtmltopdf/0.12.2.1/wkhtmltox-0.12.2.1_linux-centos6-amd64.rpm

echo "==> Installing nodeJs"
curl -sL https://rpm.nodesource.com/setup_6.x | sh -
yum install -y nodejs

echo "==> Intsalling AWS code deploy-agent, service: codedeploy-agent"
aws s3 cp s3://aws-codedeploy-us-east-1/latest/install ./install-aws-codedeploy-agent --region us-east-1
chmod +x ./install-aws-codedeploy-agent
./install-aws-codedeploy-agent auto

echo "==> Intsalling node PM2 package..."
cd /home/ec2-user
npm install -g pm2
sudo env PATH=$PATH:/usr/bin pm2 startup amazon -u ec2-user
