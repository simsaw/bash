#!/usr/bin/env bash

sh_source=https://s3.amazonaws.com/simsaw-sh

echo "== Basic Setup =="
curl $sh_source/ubuntu/basic-setup.sh | sh -

echo "== NodeJs and PM2 =="
curl $sh_source/ubuntu/node-with-pm2.sh | sh -s v4.8.3

echo "== Code Deploy =="
curl $sh_source/ubuntu/install-code-deploy-agent.sh | sh -

echo "==> Install phantomjs"
curl $sh_source/ubuntu/install-phantomjs.sh | sudo sh -

