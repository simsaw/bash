#!/bin/sh

set -e

export LC_ALL="en_US.UTF-8"
echo 'export LC_ALL="en_US.UTF8"' >> ~/.bashrc

cd /home/ec2-user

echo "==> yum update"
sudo yum -y update

echo "==> download redis"
wget http://download.redis.io/releases/redis-2.8.19.tar.gz
tar xvf redis-2.8.19.tar.gz
cd redis-2.8.19
sudo make install

#ref: http://www.saltwebsites.com/2012/install-redis-245-service-centos-6
echo "==> set redis run as service"
sudo mkdir /etc/redis /var/lib/redis
sudo cp src/redis-server src/redis-cli /usr/local/bin
sudo cp redis.conf /etc/redis
##vi /etc/redis/redis.conf:
#   daemonize yes
#   bind 127.0.0.1 (don't do this if you need to access redis from another computer)
#   loglevel notice
#   logfile /var/log/redis.log
#   dir /var/lib/redis
#   appendonly yes
## mv init-script/redis-server /etc/init.d
sudo chmod 755 /etc/init.d/redis-server
sudo chkconfig --add redis-server
sudo chkconfig --level 345 redis-server on
##vim /etc/sysctl.conf
#   vm.overcommit_memory = 1
sudo sysctl vm.overcommit_memory=1
##sudo service redis-server start

echo "==> Installing nodeJs"
curl -sL https://rpm.nodesource.com/setup_6.x | sudo sh -
sudo yum install -y nodejs

echo "==> Installing pm2"
sudo npm install -g pm2
sudo pm2 startup amazon

echo "==> Intsalling AWS code deploy-agent, service: codedeploy-agent"
aws s3 cp s3://aws-codedeploy-us-east-1/latest/install ./install-aws-codedeploy-agent --region us-east-1
chmod +x ./install-aws-codedeploy-agent
sudo ./install-aws-codedeploy-agent auto

echo "==> Install mongodb"
sudo vim /etc/yum.repos.d/mongodb.repo
#   [mongodb]
#   name=MongoDB Repository
#   baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64/
#   gpgcheck=0
#   enabled=1
sudo yum install -y mongodb-org
sudo chkconfig mongod on
sudo vim /etc/mongod.conf
#   replSet = simsaw
sudo service mongod start

echo "==> setup mongo Monitoring agent"
curl -OL https://mms.mongodb.com/download/agent/monitoring/mongodb-mms-monitoring-agent-3.1.0.175-1.x86_64.rpm
sudo rpm -U mongodb-mms-monitoring-agent-3.1.0.175-1.x86_64.rpm
sudo vi /etc/mongodb-mms/monitoring-agent.config
#   mmsApiKey=f96e36d60cdca3edaf9c308e7307f49f
sudo service mongodb-mms-monitoring-agent start

echo "==> setup mongo Backup agent"
curl -OL https://mms.mongodb.com/download/agent/backup/mongodb-mms-backup-agent-3.2.0.262-1.x86_64.rpm
sudo rpm -U mongodb-mms-backup-agent-3.2.0.262-1.x86_64.rpm
sudo vim /etc/mongodb-mms/backup-agent.config
#   mmsApiKey=f96e36d60cdca3edaf9c308e7307f49f
sudo service mongodb-mms-backup-agent start

echo "==> install prerender"
git clone https://github.com/prerender/prerender.git
cd prerender/
npm install

echo "export PAGE_DONE_CHECK_TIMEOUT=1000" >> ~/.bashrc
echo "export PORT=3320" >> ~/.bashrc
echo "export S3_BUCKET_NAME=prerender.simsaw.com" >> ~/.bashrc

vim server.js
#   server.use(prerender.s3HtmlCache()); //uncomment this line

pm2 start prerender/server.js --name prerender
