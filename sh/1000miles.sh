#!/usr/bin/env bash

# loc: https://s3.amazonaws.com/simsaw-sh/1000miles.sh

set -e -x
sh_source=https://s3.amazonaws.com/simsaw-sh

echo "== Basic Setup =="
curl $sh_source/ubuntu/basic-setup.sh | sh -

echo "== NodeJs and PM2 =="
curl $sh_source/ubuntu/node-with-pm2.sh | sh -s v6.5.0

echo "== Code Deploy =="
curl $sh_source/ubuntu/install-code-deploy-agent.sh | sh -

echo "=== imagemagic ==="
sudo apt-get -y install imagemagick

echo "***"
echo "Done"
echo "***"
exit 0
