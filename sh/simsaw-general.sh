#!/usr/bin/env bash

sh_source=https://s3.amazonaws.com/simsaw-sh

echo "== Basic Setup =="
curl $sh_source/ubuntu/basic-setup.sh | sh -

echo "== NodeJs and PM2 =="
curl $sh_source/ubuntu/node-with-pm2.sh | sh -s v6.11.1

echo "== Code Deploy =="
curl $sh_source/ubuntu/install-code-deploy-agent.sh | sh -


echo "== Redis =="
# ref: http://tosbourn.com/install-latest-version-redis-ubuntu/
sudo add-apt-repository -y ppa:rwky/redis
sudo apt-get update
sudo apt-get install -y redis-server
sudo vim /etc/redis/redis.conf
sudo service redis-server restart

echo "== MongoDB =="
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" \
    | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org

sudo vim /etc/mongod.conf
# replSet = simsaw


echo "==> setup mongo Monitoring agent"
curl -OL https://mms.mongodb.com/download/agent/monitoring/mongodb-mms-monitoring-agent_3.3.0.183-1_amd64.deb
sudo dpkg -i mongodb-mms-monitoring-agent_3.3.0.183-1_amd64.deb
sudo sed -i 's/mmsApiKey=/mmsApiKey=f96e36d60cdca3edaf9c308e7307f49f/g' /etc/mongodb-mms/monitoring-agent.config
sudo start mongodb-mms-monitoring-agent
rm mongodb-mms-monitoring-agent_3.3.0.183-1_amd64.deb

echo "==> setup mongo Backup agent"
curl -OL https://mms.mongodb.com/download/agent/backup/mongodb-mms-backup-agent_3.4.0.273-1_amd64.deb
sudo dpkg -i mongodb-mms-backup-agent_3.4.0.273-1_amd64.deb
sudo sed -i 's/mmsApiKey=/mmsApiKey=f96e36d60cdca3edaf9c308e7307f49f/g' /etc/mongodb-mms/backup-agent.config
sudo start mongodb-mms-backup-agent
rm mongodb-mms-backup-agent_3.4.0.273-1_amd64.deb

echo "==> install prerender"
git clone https://github.com/prerender/prerender.git
cd prerender/
npm install
cd ..
echo "export PAGE_DONE_CHECK_TIMEOUT=1000" >> ~/.bashrc
echo "export PORT=3320" >> ~/.bashrc
echo "export S3_BUCKET_NAME=prerender.simsaw.com" >> ~/.bashrc
sed -i 's/\/\/ server.use(prerender.s3HtmlCache())/server.use(prerender.s3HtmlCache())/g' ./prerender/server.js
pm2 start ./prerender/server.js --name prerender
pm2 save

echo "=="
echo "== Nginx =="
echo "=="
sudo add-apt-repository -y ppa:nginx/stable
sudo apt-get update
sudo apt-get -y install nginx

echo "***"
echo "Done"
echo "***"
