#!/usr/bin/env bash

sh_source=https://s3.amazonaws.com/simsaw-sh

echo "== Basic Setup =="
curl $sh_source/ubuntu/basic-setup.sh | sh -

echo "== NodeJs and PM2 =="
curl $sh_source/ubuntu/node-with-pm2.sh | sh -s v6.11.1

echo "== Code Deploy =="
curl $sh_source/ubuntu/install-code-deploy-agent.sh | sh -


echo "=="
echo "== Nginx =="
echo "=="
sudo add-apt-repository -y ppa:nginx/stable
sudo apt-get update
sudo apt-get -y install nginx

echo "***"
echo "Done"
echo "***"
