#!/bin/sh

node_v=$1

echo "==> Install Node $node_v"
wget https://nodejs.org/dist/$node_v/node-$node_v-linux-x64.tar.gz
sudo tar -C /usr/local --strip-components 1 -xzf node-$node_v-linux-x64.tar.gz
rm node-$node_v-linux-x64.tar.gz

sudo npm install -g yarn

sudo chown -R ubuntu:ubuntu /home/ubuntu/.npm/

echo "==> Install node PM2 package..."
sudo npm install -g pm2
sudo pm2 startup

sudo chown -R ubuntu:ubuntu /home/ubuntu/.pm2/

pm2 install pm2-logrotate
pm2 set pm2-logrotate:retain 7
