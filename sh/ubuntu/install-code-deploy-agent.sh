#!/bin/sh

## Setup ref
# http://docs.aws.amazon.com/codedeploy/latest/userguide/how-to-configure-existing-instance.html#how-to-configure-existing-instance-tag
##

echo "==> Install ruby"
sudo apt-get -y install ruby

echo "==> Install aws cli"
sudo apt-get -y install awscli

# sudo service codedeploy-agent status
echo "==> Install AWS Code Deploy agent"
aws s3 cp s3://aws-codedeploy-us-east-1/latest/install ./install-aws-codedeploy-agent --region us-east-1
chmod +x ./install-aws-codedeploy-agent
sudo ./install-aws-codedeploy-agent auto
rm install-aws-codedeploy-agent

