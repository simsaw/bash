#!/usr/bin/env bash

# ref: http://tosbourn.com/install-latest-version-redis-ubuntu/

sudo add-apt-repository -y ppa:rwky/redis
sudo apt-get update
sudo apt-get install -y redis-server