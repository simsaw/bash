#!/usr/bin/env bash

node_v=v8.2.0

echo "==> Installing Node $node_v"
wget https://nodejs.org/dist/$node_v/node-$node_v-linux-x64.tar.gz
sudo tar -C /usr/local --strip-components 1 -xzf node-$node_v-linux-x64.tar.gz
rm node-$node_v-linux-x64.tar.gz

echo "==> Intsalling node PM2 package..."
npm install -g pm2

sudo env PATH=$PATH:/usr/local/bin pm2 startup [platform] -u ubuntu

#pm2 install pm2-logrotate
#pm2 set pm2-logrotate:retain 7
