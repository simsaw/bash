#!/usr/bin/env bash

sudo apt-get -y install \
    xfonts-75dpi  \
    xfonts-base

# http://wkhtmltopdf.org/downloads.html
wget http://download.gna.org/wkhtmltopdf/0.12/0.12.2.1/wkhtmltox-0.12.2.1_linux-trusty-amd64.deb
sudo dpkg -i wkhtmltox-0.12.2.1_linux-trusty-amd64.deb
rm wkhtmltox-0.12.2.1_linux-trusty-amd64.deb
