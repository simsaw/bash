#!/bin/sh

echo "LANG=en_US.UTF-8" >> /etc/default/locale
#echo "LC_MESSAGES=POSIX" >> /etc/default/locale
#
echo "LC_ALL=en_US.UTF-8" >> /etc/environment
echo "LANG=en_US.UTF-8" >> /etc/environment

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

sudo apt-get -y update
# To avoid the problem of upgrade of grub
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade


echo "==> Intsalling required packages"
sudo apt-get -y install git-core build-essential openssl
sudo apt-get -y install python
