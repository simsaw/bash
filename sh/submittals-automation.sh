#!/usr/bin/env bash

set -e -x

sh_source=https://s3.amazonaws.com/simsaw-sh

echo "== Basic Setup =="
curl $sh_source/ubuntu/basic-setup.sh | sh -

echo "== NodeJs and PM2 =="
curl $sh_source/ubuntu/node-with-pm2.sh | sh -s v8.2.0

echo "== Code Deploy =="
curl $sh_source/ubuntu/install-code-deploy-agent.sh | sh -

echo "==> Install tesseract"
sudo apt-get -y install tesseract-ocr

echo "==> Install imagemagick"
sudo apt-get -y install imagemagick

echo "==> Install poppler"
sudo apt-get -y install poppler-utils

echo "***"
echo "Done"
echo "***"
