#!/usr/bin/env bash

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

echo "LC_ALL=en_US.UTF-8" >> ~/.bash_profile
echo "LANG=en_US.UTF-8" >> ~/.bash_profile

sudo yum -y update
sudo yum -y install git-core gcc gcc-c++ make cmake openssl-devel wget
sudo yum -y install jq

node_v=v6.7.0
echo "==> Install Node $node_v"
wget https://nodejs.org/dist/$node_v/node-$node_v-linux-x64.tar.gz
sudo tar -C /usr/local --strip-components 1 -xzf node-$node_v-linux-x64.tar.gz
rm node-$node_v-linux-x64.tar.gz

echo "== npm PM2 =="
npm install -g pm2
sudo su -c "env PATH=$PATH:/usr/local/bin pm2 startup amazon -u ec2-user --hp /home/ec2-user"

pm2 install pm2-logrotate
pm2 set pm2-logrotate:retain 7

echo "== CodeDeploy =="
aws s3 cp s3://aws-codedeploy-us-east-1/latest/install ./install-aws-codedeploy-agent --region us-east-1
chmod +x ./install-aws-codedeploy-agent
sudo ./install-aws-codedeploy-agent auto
rm install-aws-codedeploy-agent
