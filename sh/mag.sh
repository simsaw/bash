#!/bin/sh

sh_source=https://s3.amazonaws.com/simsaw-sh

# apt update, Git, Build Essentials & few settings
curl $sh_source/ubuntu/basic-setup.sh | sh -

# Node Js & PM2
curl $sh_source/ubuntu/node-with-pm2.sh | sh -s v6.6.0

# Code Deploy
curl $sh_source/ubuntu/install-code-deploy-agent.sh | sh -

# JQ need to read instance tag info
sudo apt-get -y install jq
