#!/usr/bin/env bash

#curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash

#source ~/.bash_profile
#source ~/.bashrc

git clone git://github.com/creationix/nvm.git ~/.nvm
echo . ~/.nvm/nvm.sh >> ~/.bashrc
. ~/.bashrc

command -v nvm
