#!/usr/bin/env bash

node_v=$1

echo "==> Install Node $node_v"
wget https://nodejs.org/dist/$node_v/node-$node_v-linux-x64.tar.gz
tar -C /usr/local --strip-components 1 -xzf node-$node_v-linux-x64.tar.gz
rm node-$node_v-linux-x64.tar.gz
