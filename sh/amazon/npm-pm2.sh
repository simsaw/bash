#!/usr/bin/env bash

echo "==> Install node PM2 package..."
#npm install -g pm2
/usr/local/bin/npm install -g pm2

echo "==> Node: $(which node)"
echo "==> npm: $(which npm)"

su -c "env PATH=$PATH:/usr/local/bin pm2 startup amazon -u ec2-user --hp /home/ec2-user"

pm2 install pm2-logrotate
pm2 set pm2-logrotate:retain 7
