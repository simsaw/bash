#!/usr/bin/env bash

export LC_ALL="en_US.UTF-8"

echo "==> aws CodeDeploy"
aws s3 cp s3://aws-codedeploy-us-east-1/latest/install ./install-aws-codedeploy-agent --region us-east-1
chmod +x ./install-aws-codedeploy-agent
./install-aws-codedeploy-agent auto
rm install-aws-codedeploy-agent

# sudo service codedeploy-agent status
