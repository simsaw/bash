#!/usr/bin/env bash

sudo yum groupinstall 'Development Tools' && sudo yum install curl git irb python-setuptools ruby
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install)"

echo 'export PATH="$HOME/.linuxbrew/bin:$HOME/.linuxbrew/sbin:$PATH"' >> ~/.bash_profile
echo 'export MANPATH="$HOME/.linuxbrew/share/man:$MANPATH"' >> ~/.bash_profile
echo 'export INFOPATH="$HOME/.linuxbrew/share/info:$INFOPATH"' >> ~/.bash_profile
source ~/.bash_profile

ln -s /usr/bin/gcc48 `brew --prefix`/bin/gcc-4.8
#ln -s /usr/bin/g++ `brew --prefix`/bin/g++

brew install hello
brew doctor
