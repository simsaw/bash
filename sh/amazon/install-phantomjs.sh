#!/usr/bin/env bash

V=phantomjs-1.9.8

echo "==> Install phantomjs"
sudo yum install fontconfig freetype freetype-devel fontconfig-devel libstdc++
wget https://bitbucket.org/ariya/phantomjs/downloads/$V-linux-x86_64.tar.bz2
sudo mkdir -p /opt/phantomjs
bzip2 -d $V-linux-x86_64.tar.bz2
sudo tar -xvf $V-linux-x86_64.tar --directory /opt/phantomjs/ --strip-components 1
sudo ln -s /opt/phantomjs/bin/phantomjs /usr/bin/phantomjs
rm $V-linux-x86_64.tar
