#!/bin/sh

export LC_ALL="en_US.UTF-8"
echo 'export LC_ALL="en_US.UTF-8"' >> ~/.bashrc

cd /home/ec2-user

echo "==> yum update"
sudo yum update -y

echo "==> Intsalling required packages"
sudo yum install -y git-core gcc gcc-c++ make cmake openssl-devel

echo "==> Installing NVM"
node_v=v0.12.2
curl https://raw.githubusercontent.com/creationix/nvm/v0.24.0/install.sh | bash
source $HOME/.bashrc
echo "==> Installing Node $node_v"
nvm install $node_v
nvm alias default $node_v

echo "==> Intsalling AWS code deploy-agent, service: codedeploy-agent"
aws s3 cp s3://aws-codedeploy-us-east-1/latest/install ./install-aws-codedeploy-agent --region us-east-1
chmod +x ./install-aws-codedeploy-agent
sudo ./install-aws-codedeploy-agent auto
rm install-aws-codedeploy-agent

echo "==> Intsalling node PM2 package..."
npm install -g pm2
sudo env PATH=$PATH:/home/ec2-user/.nvm/versions/node/v0.12.0/bin pm2 startup amazon -u ec2-user

# copy lib so that all user has access to nodejs, pm2
n=$(which node);n=${n%/bin/node}; chmod -R 755 $n/bin/*; sudo cp -r $n/{bin,lib,share} /usr #/usr/local

echo "Install poppler"
#sudo yum -y instal poppler
wget http://cznic.dl.sourceforge.net/project/openjpeg.mirror/2.1.0/openjpeg-2.1.0.tar.gz
tar xf openjpeg-2.1.0.tar.gz
rm openjpeg-2.1.0.tar.gz
cd openjpeg-2.1.0
cmake .
make
sudo make install
cd
rm -rf openjpeg-2.1.0

wget http://poppler.freedesktop.org/poppler-0.32.0.tar.xz
tar xf poppler-0.32.0.tar.xz
rm poppler-0.32.0.tar.xz
cd poppler-0.32.0
cmake .

echo "Install GhostScript"
wget http://downloads.ghostscript.com/public/ghostscript-9.16.tar.gz
tar xvf ghostscript-9.16.tar.gz
cd ghostscript-9.16
./configure
make
sudo make install
cd ..
rm -rf ghostscript-9.16
rm ghostscript-9.16.tar.gz

echo "Intsall ImageMagick"
wget http://www.imagemagick.org/download/ImageMagick.tar.gz
tar xvzf ImageMagick.tar.gz
cd ImageMagick-6.9.1-2
./configure
make
sudo make install
sudo ldconfig /usr/local/lib
cd
rm ImageMagick-6.9.1-2

echo "Install tesseract"
# install leptonica, tesseract neeed it.
wget http://www.leptonica.com/source/leptonica-1.72.tar.gz
tar xvf leptonica-1.72.tar.gz
rm leptonica-1.72.tar.gz
cd leptonica-1.72
./configure
make
sudo make install
cd
rm -rf cd leptonica-1.72
# install tesseract
wget https://tesseract-ocr.googlecode.com/files/tesseract-ocr-3.02.02.tar.gz
tar xvf tesseract-ocr-3.02.02.tar.gz
rm tesseract-ocr-3.02.02.tar.gz
cd tesseract-ocr
./configure
make
sudo make install
cd
rm -rf tesseract-ocr

#echo "Install exiftool"
##ExtUtils is needed, install it using perl-Tk-devel
#sudo yum install perl-Tk-devel
##Get Exif code
#wget http://www.sno.phy.queensu.ca/~phil/exiftool/Image-ExifTool-9.93.tar.gz
#gzip -dc Image-ExifTool-9.93.tar.gz | tar -xf -
#rm Image-ExifTool-9.93.tar.gz
#cd Image-ExifTool-9.93
#perl Makefile.PL
#make test
#sudo make install
#cd
#rm -rf Image-ExifTool-9.93


echo "Install pdfinfo"
wget ftp://ftp.foolabs.com/pub/xpdf/xpdfbin-linux-3.04.tar.gz
tar xf xpdfbin-linux-3.04.tar.gz
rm xpdfbin-linux-3.04.tar.gz
cd xpdfbin-linux-3.04
sudo cp bin64/pdfinfo /usr/local/bin/

rm -rf xpdfbin-linux-3.04
