#!/usr/bin/env bash

# ref http://alangunning.ie/2013/01/13/how-to-install-tesseract-ocr-on-a-amazon-ec2-free-tier-linux-machine-for-optical-character-recognition/

sudo yum update

sudo yum install gcc gcc-c++ make
sudo yum install autoconf aclocal automake
sudo yum install libtool
sudo yum install libjpeg-devel libpng-devel libtiff-devel zlib-devel

cd ~
mkdir leptonica
cd leptonica
wget http://www.leptonica.com/source/leptonica-1.69.tar.gz
tar -zxvf leptonica-1.69.tar.gz
cd leptonica-1.69
./configure
make  # Takes ~20 minutes on T1 Micro Instance machine
sudo make install


cd ~
mkdir tesseract
cd tesseract
wget http://tesseract-ocr.googlecode.com/files/tesseract-ocr-3.02.02.tar.gz
tar -zxvf tesseract-ocr-3.02.02.tar.gz
cd tesseract-ocr
./autogen.sh
./configure
make  # Takes ~40 minutes on T1 Micro Instance machine
sudo make install
sudo ldconfig


cd /usr/local/share/tessdata
sudo wget http://tesseract-ocr.googlecode.com/files/tesseract-ocr-3.02.eng.tar.gz
sudo tar xvf tesseract-ocr-3.02.eng.tar.gz
export TESSDATA_PREFIX=/usr/local/share/
sudo mv tesseract-ocr/tessdata/* .

echo 'export TESSDATA_PREFIX=/usr/local/share/' >>  ~/.bash_profile
