#!/usr/bin/env bash

# loc: https://s3.amazonaws.com/simsaw-sh/simsaw-baas.sh

set -e -x

sh_source=https://s3.amazonaws.com/simsaw-sh

echo "== Basic Setup =="
curl $sh_source/ubuntu/basic-setup.sh | sh -

echo "== NodeJs and PM2 =="
curl $sh_source/ubuntu/node-with-pm2.sh | sh -s v6.11.1


echo "== Code Deploy =="
curl $sh_source/ubuntu/install-code-deploy-agent.sh | sh -


echo "== wkhtmltopdf =="
sudo apt-get -y install \
    xfonts-75dpi  \
    xfonts-base
# http://wkhtmltopdf.org/downloads.html
wget http://softlayer-sng.dl.sourceforge.net/project/wkhtmltopdf/0.12.2.1/wkhtmltox-0.12.2.1_linux-trusty-amd64.deb
sudo dpkg -i wkhtmltox-0.12.2.1_linux-trusty-amd64.deb

echo "=== imagemagic ==="
sudo apt-get -y install imagemagick

echo "***"
echo "Done"
echo "***"
exit 0
